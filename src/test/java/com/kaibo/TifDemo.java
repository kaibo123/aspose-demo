package com.kaibo;

import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.ImageEncoder;
import com.sun.media.jai.codec.PNGEncodeParam;
import com.sun.media.jai.codec.TIFFDecodeParam;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author kaibo
 * @date 2021/7/29 11:30
 * @GitHub：https://github.com/yuxuelian
 * @email：kaibo1hao@gmail.com
 * @description：
 */

public class TifDemo {

    @Test
    public void test() {
        String tiff = "C:\\Users\\admin\\Desktop\\456.tif";
        String png = "C:\\Users\\admin\\Desktop\\aaa2.png";
        try (InputStream inputStream = new FileInputStream(tiff);
             OutputStream outputStream = new FileOutputStream(png)) {
            ImageDecoder decoder = ImageCodec.createImageDecoder("tiff", inputStream, new TIFFDecodeParam());
            ImageEncoder encoder = ImageCodec.createImageEncoder("png", outputStream, new PNGEncodeParam.RGB());
            encoder.encode(decoder.decodeAsRenderedImage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
