package com.kaibo;

import org.junit.Test;

import java.io.File;

public class JavaTest {

    @Test
    public void word2PdfTest() {
//        File wordFile = new File("test-file/test.docx");
//        File pdfFile = new File("test-file/test.docx.pdf");
//        Word2PdfUtil.word2Pdf(wordFile, pdfFile);

        File wordFile = new File("test-file/test.txt");
        File pdfFile = new File("test-file/test.txt.pdf");
        Word2PdfUtil.word2Pdf(wordFile, pdfFile);
    }

    @Test
    public void excel2PdfTest() {
        File excelFile = new File("test-file/test.xlsx");
        File pdfFile = new File("test-file/test.xlsx.pdf");
        Excel2PdfUtil.excel2Pdf(excelFile, pdfFile);
    }

    @Test
    public void ppt2PdfTest() {
        File pptFile = new File("test-file/test.pptx");
        File pdfFile = new File("test-file/test.pptx.pdf");
        Ppt2PdfUtil.ppt2Pdf(pptFile, pdfFile);
    }

    @Test
    public void txt2PdfTest() {
        File txtFile = new File("test-file/test.txt");
        File pdfFile = new File("test-file/test.txt.pdf");
        Txt2PdfUtils.txt2Pdf(txtFile, pdfFile);
    }

}
