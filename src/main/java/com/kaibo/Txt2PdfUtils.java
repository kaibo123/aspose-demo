package com.kaibo;

import java.io.File;

/**
 * @author kaibo
 * @date 2021/8/10 20:27
 * @GitHub：https://github.com/yuxuelian
 * @email：kaibo1hao@gmail.com
 * @description：
 */

public class Txt2PdfUtils {


    /**
     * ppt 2 pdf.
     *
     * @param txtFile
     * @param pdfFile
     */
    public static void txt2Pdf(File txtFile, File pdfFile) {
        Word2PdfUtil.word2Pdf(txtFile, pdfFile);
    }


}


