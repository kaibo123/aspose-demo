package com.kaibo;

/**
 * @author: kaibo
 * @date: 2020/8/28 0:20
 * @GitHub: https://github.com/yuxuelian
 * @qq: 568966289
 * @description:
 */

import com.aspose.cells.License;
import com.aspose.cells.SaveFormat;
import com.aspose.cells.Workbook;

import java.io.*;

public class Excel2PdfUtil {

    private static void initLicense() {
        try {
            InputStream license = new ByteArrayInputStream(("<License>" +
                    "<Data>" +
                    "<Products>" +
                    "<Product>Aspose.Total for Java</Product>" +
                    "<Product>Aspose.excels for Java</Product>" +
                    "</Products>" +
                    "<EditionType>Enterprise</EditionType>" +
                    "<SubscriptionExpiry>20991231</SubscriptionExpiry>" +
                    "<LicenseExpiry>29991231</LicenseExpiry>" +
                    "<SerialNumber>8bfe198c-7f0c-4ef8-8ff0-acc3237bf0d7</SerialNumber>" +
                    "</Data>" +
                    "<Signature>sNLLKGMUdF0r8O1kKilWAGdgfs2BvJb/2Xp8p5iuDVfZXmhppo+d0Ran1P9TKdjV4ABwAgKXxJ3jcQTqE/2IRfqwnPf8itN8aFZlV3TJPYeD3yWE7IT55Gz6EijUpC7aKeoohTb4w2fpox58wWoF3SNp6sK6jDfiAUGEHYJ9pjU=</Signature>" +
                    "</License>").getBytes());
            new License().setLicense(license);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static {
        initLicense();
    }

    /**
     * excel 2 pdf.
     *
     * @param excelFile
     * @param pdfFile
     */
    public static void excel2Pdf(File excelFile, File pdfFile) {
        try (InputStream inputStream = new FileInputStream(excelFile);
             OutputStream outputStream = new FileOutputStream(pdfFile)) {
            Workbook workbook = new Workbook(inputStream);
            // 保存转换的pdf文件
            workbook.save(outputStream, SaveFormat.PDF);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}