package com.kaibo;

/**
 * @author: kaibo
 * @date: 2020/8/28 0:20
 * @GitHub: https://github.com/yuxuelian
 * @qq: 568966289
 * @description:
 */

import com.aspose.slides.License;
import com.aspose.slides.Presentation;
import com.aspose.slides.SaveFormat;

import java.io.*;

public class Ppt2PdfUtil {

    private static void initLicense() {
        try {
            InputStream license = new ByteArrayInputStream(("<License>" +
                    "<Data>" +
                    "<Products>" +
                    "<Product>Aspose.Total for Java</Product>" +
                    "<Product>Aspose.Slides for Java</Product>" +
                    "</Products>" +
                    "<EditionType>Enterprise</EditionType>" +
                    "<SubscriptionExpiry>20991231</SubscriptionExpiry>" +
                    "<LicenseExpiry>29991231</LicenseExpiry>" +
                    "<SerialNumber>8bfe198c-7f0c-4ef8-8ff0-acc3237bf0d7</SerialNumber>" +
                    "</Data>" +
                    "<Signature>sNLLKGMUdF0r8O1kKilWAGdgfs2BvJb/2Xp8p5iuDVfZXmhppo+d0Ran1P9TKdjV4ABwAgKXxJ3jcQTqE/2IRfqwnPf8itN8aFZlV3TJPYeD3yWE7IT55Gz6EijUpC7aKeoohTb4w2fpox58wWoF3SNp6sK6jDfiAUGEHYJ9pjU=</Signature>" +
                    "</License>").getBytes());
            new License().setLicense(license);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static {
        initLicense();
    }

    /**
     * ppt 2 pdf.
     *
     * @param pptFile
     * @param pdfFile
     */
    public static void ppt2Pdf(File pptFile, File pdfFile) {
        try (InputStream inputStream = new FileInputStream(pptFile);
             OutputStream outputStream = new FileOutputStream(pdfFile)) {
            Presentation presentation = new Presentation(inputStream);
            presentation.save(outputStream, SaveFormat.Pdf);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}